# README

This project contains a simple component library with a consumer app.

You can find the project online at https://wt-spinner.surge.sh/.

Following, you can why I took some decisions and points of improvement.

# Motivations

## Usage of styled components

Styles need to be updated based on input. Because this js-css dependency it makes sense to go for styled components. As a plus, style-encapsulation is for free and the consumer app does not need to adapt (e.g. manage sass compilation).

I found out that is better to have a separate file for all the styled components, it makes the core component easier to read. Usually I prefer a flat folder component structure, but I decided to nest the folder mainly because of the style additional file. That may sound a bit overkill but as a component gets README, storybook and more complexity a clean folder structure pays off.

## Color handling

The application should have total control over the widget colors, plus these colors should be standardized in some way to avoid random and not reused colors floating around. I decided to give the spinner more inputs and have the colors as constants inside the app itself (could be also inside the library itself if using a design system).

## Lib folder and structure

Components and animations are stored in the "lib" folder, which has a modular structure and exports everything available to the app. The app does not need to have knowledge about the structure of the lib itself, just about what it provides.

## Testing

Tests should be based on consumer expectations. Using the spinner, the expectations are the following:

- the given progress is displayed somewhere
- if the spinner is active, the animation should be running
- if the spinner is not active, the animation should be paused
- if the spinner activity is not defined, the animation should be paused

We could add more tests based on snapshots (e.g. enzyme)/screeshots, but I prefer to focus on the core behaviour of the component first. Sometimes, having specific asserts is more robust than have a test break if one line of css is updated.
I decided to test just the spinner, other components testing would be quite similar (just props and rendering, no api calls).

About the unit test code, I usually split in three parts: setup, execute, assert. It's a good framework to follow to have readable tests.

## Components

While doing the spinner I thought that is quite common to have a value with an unit, and would be great to have it standardized in the UI. That's why I created a flexible Measurement component to handle this use case.

## Prop types

I was in doubt about using prop-types. Anyway, I noticed that typescript was already giving most of what I needed for free so I decided not to add another package.

# Improvements

- **Spinner size**: the sizing of the spinner is fixed. To make it variable, things like the progress font size need to be taken into account. Would be great to just provide a couple of available sizes for the spinner (e.g. S/M/L/XL) and adjust accordingly.
- **Spinner math**: there is some general math in the component itself which could be wrapped in utils functions to be reused. This would also make the code more readable.
- **Lib documentation**: add for consumers on README files with storybook stories to play with components
- **Lib default colors**: if this library would be used by multiple apps which have the same color scheme, I would move the colors to the library itself and add defaults to the component (so there is no need to input everything or create wrapper in the app itself)
