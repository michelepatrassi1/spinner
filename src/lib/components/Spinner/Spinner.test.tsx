import React from "react";
import { render, screen } from "@testing-library/react";
import { Spinner, SpinnerProps, testIds } from "./Spinner";

const props: SpinnerProps = {
  progress: 50,
  color: "red",
  bgColor: "blue",
  textColor: "green",
};

describe("Spinner", () => {
  it("should show the given progress", () => {
    render(<Spinner {...props} />);

    const progressEl = screen.getByText(`${props.progress}`);

    expect(progressEl).toBeInTheDocument();
  });

  describe("isActive", () => {
    const testId = testIds.animatedCircle;

    it("should not animate by default", () => {
      render(<Spinner {...props} />);

      const el = screen.getByTestId(testId);

      expect(el).toHaveStyle(`animation-play-state: paused`);
    });

    it("should not animate when isActive is false", () => {
      render(<Spinner {...props} isActive={false} />);

      const el = screen.getByTestId(testId);

      expect(el).toHaveStyle(`animation-play-state: paused`);
    });

    it("should animate when isActive is true", () => {
      render(<Spinner {...props} isActive={true} />);

      const el = screen.getByTestId(testId);

      expect(el).toHaveStyle(`animation-play-state: running`);
    });
  });
});
