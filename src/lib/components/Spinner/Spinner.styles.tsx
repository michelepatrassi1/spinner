import styled from "styled-components";
import { rotate } from "../../animations";

export const Svg = styled.svg<{ size: number }>`
  width: ${({ size }) => size}px;
  height: ${({ size }) => size}px;
`;

export const Circle = styled.circle<{
  isActive: boolean;
  circumference: number;
}>`
  stroke-dasharray: ${({ circumference }) => circumference}px;
  stroke-dashoffset: ${({ strokeDashoffset }) => strokeDashoffset}px;
  stroke-width: ${({ strokeWidth }) => strokeWidth}px;
  fill: transparent;
  transform-origin: center;
  stroke: ${({ stroke }) => stroke};
  animation: ${rotate} 1s linear infinite;
  stroke-linecap: ${({ strokeLinecap }) =>
    strokeLinecap ? strokeLinecap : "butt"};
  animation-play-state: ${({ isActive }) => (isActive ? "running" : "paused")};
`;

export const Center = styled.span`
  position: absolute;
  left: 0;
  top: 0;
  bottom: 0;
  right: 0;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const Wrapper = styled.div`
  position: relative;
`;

export const MeasurementWrapper = styled.span<{ marginLeft: string }>`
  position: relative;
  margin-left: ${({ marginLeft }) => (marginLeft ? marginLeft : 0)};
`;
