import React, { ReactElement } from "react";
import { Measurement } from "..";
import {
  Wrapper,
  Center,
  MeasurementWrapper,
  Svg,
  Circle,
} from "./Spinner.styles";

export interface SpinnerProps {
  progress: number;
  color: string;
  bgColor: string;
  textColor: string;
  isActive?: boolean;
}

export const testIds = {
  animatedCircle: "animated-circle",
};

export const Spinner = ({
  progress,
  isActive = false,
  color,
  bgColor,
  textColor,
}: SpinnerProps): ReactElement => {
  const diameter = 100;
  const strokeWidth = 10;
  const radius = (diameter - strokeWidth) / 2;
  const circumference = 2 * Math.PI * radius;
  const strokeDashoffset = (circumference * (100 - progress)) / 100;
  const circleCoordinate = 50;

  const viewBox = radius * 2 + strokeWidth;
  const viewBoxProp = `0 0 ${viewBox} ${viewBox}`;

  const percentageOffset = `${radius / 4}px`;

  return (
    <Wrapper>
      <Center>
        <MeasurementWrapper marginLeft={percentageOffset}>
          <Measurement color={textColor} symbol={"%"} symbolColor={bgColor}>
            {progress}
          </Measurement>
        </MeasurementWrapper>
      </Center>
      <Svg
        viewBox={viewBoxProp}
        size={diameter}
        preserveAspectRatio="xMidYMid meet"
      >
        <Circle
          cx={circleCoordinate}
          cy={circleCoordinate}
          r={radius}
          isActive={false}
          stroke={bgColor}
          circumference={circumference}
          strokeWidth={strokeWidth}
        />
        <Circle
          data-testid={testIds.animatedCircle}
          cx={circleCoordinate}
          cy={circleCoordinate}
          r={radius}
          strokeDashoffset={strokeDashoffset}
          isActive={isActive}
          stroke={color}
          strokeLinecap={"round"}
          circumference={circumference}
          strokeWidth={strokeWidth}
        />
      </Svg>
    </Wrapper>
  );
};
