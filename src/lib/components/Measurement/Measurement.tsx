import React, { ReactElement } from "react";
import { Wrapper, Value, Symbol } from "./Measurements.styles";

export const Measurement = ({
  children,
  symbolColor,
  color,
  symbol,
}: {
  children: string | number;
  symbolColor: string;
  color: string;
  symbol: string;
}): ReactElement => {
  return (
    <Wrapper>
      <Value color={color}>{children}</Value>
      <Symbol color={symbolColor}>{symbol}</Symbol>
    </Wrapper>
  );
};
