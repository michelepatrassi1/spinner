import styled from "styled-components";

export const Wrapper = styled.div`
  display: flex;
`;

export const Value = styled.span`
  color: ${({ color }) => color};
  font-size: 1.5rem;
  font-weight: 500;
`;

export const Symbol = styled.span`
  color: ${({ color }) => color};
  font-weight: bold;
  font-size: 0.75rem;
`;
