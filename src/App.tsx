import React, { useState } from "react";
import "./App.css";
import { Spinner } from "./lib";
import {
  COLOR_PRIMARY,
  COLOR_SECONDARY,
  COLOR_TEXT_PRIMARY,
} from "./constants";

function App() {
  const [progress, setProgress] = useState(50);
  const [isActive, setIsActive] = useState(true);

  return (
    <div className="App">
      <Spinner
        color={COLOR_PRIMARY}
        bgColor={COLOR_SECONDARY}
        textColor={COLOR_TEXT_PRIMARY}
        progress={progress}
        isActive={isActive}
      />
      <div className="App-controls">
        <h1>Control center 🎮</h1>
        <div className="App-controls-range">
          <input
            type="range"
            id="progress"
            name="progress"
            min="0"
            max="100"
            value={progress}
            onChange={(event) => setProgress(parseInt(event.target.value))}
          />
          <label htmlFor="progress">Progress</label>
        </div>
        <div className="App-controls-buttons">
          <button onClick={() => setIsActive(true)}>Start!</button>
          <button onClick={() => setIsActive(false)}>Stop</button>
        </div>
      </div>
    </div>
  );
}

export default App;
